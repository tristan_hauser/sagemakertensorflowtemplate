#!/bin/bash

#        >>NAME IMAGE: e.g., python/sagemaker-1234<<
IMAGELAB=___________________________________________
BASEDIR=$( pwd )
DATADIR=$BASEDIR/Data
CONTAINER=$BASEDIR/Containers/Train
#   >>CHOOSE UNIQUE ID TO SPECIFY CONTAINER<<
TAG=_________________________________________

## Specific settings for login, label and push commands are
## optained by creating an AWS ECR Repository and then
## using the output of the "View push commands" button.
#      >>COPY IN REPO LABEL: e.g. 1234.dkr.erc.us-east-1.amazonaws.com<<
AWSLAB=_________________________________________________________________

## Log into the Amazon Elastic Container Registry
## ----------------------------------------------
## Need to run this if planning to push container to AWS to use their
## optimization service.
## Following line copied from the aws ecr push commands for target repo
#| aws ecr get-login-password --region us-east-1 \
#|     | docker login --username AWS \
#|              --password-stdin $AWSLAB
## The following seems to work more consistantly though:
login=$( aws ecr get-login --no-include-email --region us-east-1 )
$login

## Prepare directory
## -----------------
## All scripts that want to run has to within the directory that will run
## docker in for the program to be able to find it.
cp train-model.py $CONTAINER/
cp Dockerfile $CONTAINER/
cd $CONTAINER

## Build image
## -----------
## Building the image runs the operations in the `Dockerfile`
## Stores the set up "on file" so can be run when needed. 
docker build -t $IMAGELAB .

## Send to AWS
## -----------
## Give the image a unique label and push to the online training service
docker tag $IMAGELAB:latest $AWSLAB/$IMAGELAB:$TAG
docker push $AWSLAB/$IMAGELAB:$TAG

## Run local (opt)
## ---------------
## Run the container on local machine, setting various variables,
## eg. paths to data and state command to run.
#| docker run                               \
#|        --rm                              \
#|        -v $DATADIR/OUTPUT:/opt/ml/output \
#|        -v $DATADIR:/data                 \
#|        -e SM_CHANNEL_TRAIN=/data/TRAIN   \
#|        -e SM_CHANNEL_TEST=/data/TEST     \
#|        sagemaker/test001 python /opt/ml/code/train.py

## If want to pull files from the container after it runs then
## don't use the `--rm` flag, so that the container stays avalible,
## and then remove the congtainer after moving files
## (in this example the script produces a plot called `model_fit.png`).
#| ID=$( docker ps -aqf "ancestor=$IMAGELAB" )		
#| docker cp $ID:/opt/ml/output/model_fit.png .		
#| docker rm $ID                        
