
""" Run remote training

    =================== 
    Specific AWS settings are optained by creating an 
    AWS ECR Repository and then using the output of the 
    "View push commands" button.
    
    To start the training procedure run this script 
    with the desired settings; e.g.,
    $ python run-sagemaker.py \
          --type tune --spot  \
          --instance_type ml.m5.large \
          --job_name regtest0004
    """

import sagemaker.estimator as sg_estimator
import sagemaker.tuner as sg_tuner
import sagemaker.parameter as sg_par
import sagemaker.session as sg_sess
import argparse
import logging
import boto3 

## Specific id codes and lables are set through users AWS account
#  >> THESE NAMES NEED TO MATCH THOSE GIVEN IN build-container.sh<<
AWSCODE  = _____________________
AWSLAB   = _____________________
IMAGELAB = _____________________

""" Set logging 
    ----------- """
logger = logging.getLogger('sagemaker_log')
log_handler = logging.FileHandler('sagemaker.log')
log_formatter = logging.Formatter('%(asctime)s %(message)s')
log_handler.setFormatter(log_formatter)
logger.addHandler(log_handler)
logger.setLevel(logging.INFO)

""" Define argument flags 
    --------------------- """
parser = argparse.ArgumentParser(
    description="Run Sagemaker jobs to train, tune GHI models"
)
## Sagemaker flags
parser.add_argument(
    '--type',
    type=str,
    default='fit',
    help='Select `fit` to fit a single model or `tune` to auto-optimize'
)
parser.add_argument(
    '--spot',
    action='store_true',
    help='Use AWS spot instances'
)
parser.add_argument(
    '--instance_type',
    type=str,
    default='local',
    help=(
        "Select AWS instance type, full list at end of file "
        + "if set to local, runs on current machine and"
        + "can not use `tune` option"
    )
)
parser.add_argument(
    '--Timeout_Hours',
    type=int,
    default=1,
    help='How many hours to allow the job to run'
)
parser.add_argument(
    '--max_jobs',
    type=int,
    default=4,
    help=(
        'How many jobs should be created for hyperparameter search ' 
        + 'remember that more jobs equals higher cost'
    )
)
parser.add_argument(
    '--max_parallel_jobs',
    type=int,
    default=2,
    help='How many parallel hyperparameter jobs should run at once?'
)
parser.add_argument(
    '--job_name',
    type=str,
    default='unnamed',
    help='Name the training/tuning job'
)
## Model hyper parameters if doing fixed training run
parser.add_argument(
    '--number_nodes',
    type=int,
    default=1,
    help="number of nodes in hidden layer"
)
parser.add_argument(
    '--activation_function',
    type=str,
    default='tanh',
    help="short hand for network activation function to use; "
         + "e.g., relu, tanh, sigmoid"
)
args = parser.parse_args()
args = vars(args)

""" Validate arguments
    ------------------
    Can add some checks so that exceptions get raised if the provided
    arguments aren't in the expected form.
    """
if args['type'].upper() in ['FIT','TUNE']:
    # also stores value as variable
    TYPE = args['type'].upper()
    logger.info(" Selected {0} Job".format(TYPE))
else:
    raise Exception(
        "Invalid job type {0}: "
        + "please select either Fit or Tune".format(args['type'])
    )


""" Assign hyperparameters
    ----------------------
    Model specific arguments are stored in a dictionary and
    are passed to the script that defines and trains
    the actual model as commandline arguments: `--hyperparameter value`

    The `HyperParameterRanges` dictionary defines values to be explored
    by the optimization program
    """
HyperParameters = {}
HyperParameters['number_nodes'] = int(args['number_nodes'])
HyperParameters['activation_function'] = str(args['activation_function'])
HyperParameterRanges = {
    'number_nodes' : sg_par.IntegerParameter(
        10,100,scaling_type='Auto'
    ),
    'activation_function' : sg_par.CategoricalParameter(
        ['relu','tanh','sigmoid']
    )
}

""" Set container and AWS specifics
    ------------------------------- """
# >> THIS NAME NEEDS TO MATCH THE "tag" GIVEN IN build-container.sh<<
ContainerVersion = ____________________
img = AWSLAB+'/'+IMAGELAB+':'+'{0}'.format(ContainerVersion)
Sagemaker_Role = 'arn:aws:iam::'+AWSCODE+':role/sagemaker-training'
print('Sagemaker_Role : '+Sagemaker_Role)

""" Set data paths
    -------------- 
    The data paths here are set on s3 storage, can create folders, 
    and find related paths using the AWS Management Console.
    """

# >> HERE GIVE PATHS TO PRESET S3 STORAGE LOCATIONS <<
DATAPATHS = {
    'TRAIN' : ___________________________________,
    'TEST'  : ___________________________________,
    'OUT'   : ___________________________________
}
INPUTS = {'TRAIN':DATAPATHS['TRAIN'], 'TEST':DATAPATHS['TEST']}

logger.info(
    " TRAIN Input {0}\n TEST Input {1} ".format(
        DATAPATHS['TRAIN'],DATAPATHS['TEST']
    )
)
S3_OUTPUT = DATAPATHS['OUT']+'/{job_name}'.format(job_name=args['job_name'])
logger.info(" Output location : {0}".format(S3_OUTPUT)) 
if not args['instance_type'] == 'local':
    sess = sg_sess.Session(
        boto3.session.Session(),
        boto3.client('sagemaker'),
#       default_bucket='nimbus-ml'
    )

else:
    sess = None

""" Create the estimator
    --------------------
    Define the runing of the model (training) from the docker image
    Tuning will be done based on values written to standard out by the
    training script. Need to be very careful that the output and the 
    regex defined here to find them match.
    """
estimator = sg_estimator.Estimator(
    img, Sagemaker_Role, 1,
    args['instance_type'],
    train_max_run=args['Timeout_Hours']*60*60, 
    input_mode='File', 
    output_path=S3_OUTPUT, 
    hyperparameters=HyperParameters, 
    sagemaker_session=sess, 
    train_use_spot_instances=args['spot'], 
    train_max_wait=args['Timeout_Hours']*60*60
)

if TYPE == 'FIT' :
    logger.info(" EXECUTE FIT JOB{0}".format(args['job_name']))
elif TYPE == 'TUNE' :
    Metrics = [
        {'Name': 'MAE',  'Regex': 'RESULT MAE: ([-.0-9 ]*)'},
        {'Name': 'STD',  'Regex': 'RESULT STD: ([-.0-9 ]*)'},
        {'Name': 'CONF', 'Regex': 'RESULT CONF: ([-.0-9 ]*)'},
        {'Name': 'COR',  'Regex': 'RESULT COR: ([-.0-9 ]*)'}
    ]
    tune = sg_tuner.HyperparameterTuner(
        estimator,'MAE',HyperParameterRanges,
        metric_definitions=Metrics,
        objective_type='Minimize',
        max_jobs=args['max_jobs'],
        max_parallel_jobs=args['max_parallel_jobs'],
        tags=[{"Key":"ProjectNumber","Value":'Sagemaker'},
              {"Key":"Contact","Value":"steven.beale@woodplc.com"}]
    )
    logger.info(" EXECUTE TUNE JOB {0}".format(args['job_name']))
    tune.fit(job_name=args['job_name'],inputs=INPUTS)
        
    
