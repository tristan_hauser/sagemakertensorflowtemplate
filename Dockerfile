FROM python:3.6
RUN apt-get update

## Establish sagemaker setup: provides numpy, scipy
## ------------------------------------------------
RUN pip3 install sagemaker-containers

## Add other modules
## -----------------
## Note: If you want to run training on gpu instance,
##       switch tensorflow to tensorflow-gpu.
RUN pip3 install tensorflow \
                 tensorflow-probability \
                 xgboost \
                 sklearn \
                 pandas \
                 joblib \
                 seaborn
		 
## Setup in container directory structure
## --------------------------------------
## Sagemaker makes assumptions about the directory
## structure of the containers it runs, so need to
## follow those patterns.
RUN mkdir -p /opt/ml/input
RUN mkdir -p /opt/ml/output
RUN mkdir -p /opt/ml/model
RUN mkdir -p /opt/ml/code
RUN mkdir -p /opt/ml/output
RUN mkdir -p /opt/ml/code/lib

## Copy training code 
## ------------------ 
COPY train-model.py /opt/ml/code/train.py

## Set training executable
## -----------------------
ENV SAGEMAKER_PROGRAM train.py
