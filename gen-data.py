""" Create file of pseudo data
    ========================== """
import pandas as pd
import numpy as np

def data_gen(train_size,sigma=1.0,bounds=(-0.5,0.5)) :
    '''Define data shape
    
    Variables
    ---------
    train_size : number of samples
    sigma : noise level
    bounds : min and max independent variable (tuple)
    '''
    X = np.linspace(
        bounds[0], bounds[1], train_size
    ).reshape(-1, 1)
    epsilon = np.random.randn(*X.shape) * sigma
    y = 10 * np.sin(2 * np.pi * (X)) + epsilon
    return(X,y)

# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>:: #

train_size = 32
x0,y0 = data_gen(train_size)
X0 = pd.DataFrame(
    np.column_stack([x0,y0]),
    index=range(len(x0)),
    columns=['Input','Target']
)
X0.to_csv('./Data/TRAIN/training_data.csv',index=False)

test_size = 100 
x1,y1 = data_gen(test_size,bounds=(-0.7,0.7))
X1 = pd.DataFrame(
    np.column_stack([x1,y1]),
    index=range(len(x1)),
    columns=['Input','Target']
)
X1.to_csv('./Data/TEST/test_data.csv',index=False)

# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>:: #
