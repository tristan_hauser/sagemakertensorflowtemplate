""" Create a simple NN
    ================== """
## Machine Learning
import tensorflow_probability as tfp
import tensorflow as tf
tfd = tfp.distributions
## Make network elements default to 64bit floats
tf.keras.backend.set_floatx('float64')
## Math/data 
import pandas as pd
import numpy as np
## System interaction
import argparse
import os
## Visulization
import matplotlib.pyplot as plt
import seaborn as sns
_ = sns.set_style('darkgrid')
_ = sns.set_palette('Set3')
cpal = sns.color_palette('Set3')

""" The output directory name has to match both the container set up that gets
    performed locally, as well as where the SageMaker program expects the 
    information that it is going to store will be. 
    """
OUTDIR = '/opt/ml/model/'

""" Set up
    ------ 
    Free parameters are passed using command line flags.
    
    These arguments are then stored in a `HyperParemeter`
    dictionary. This step is optional, but makes it easier to pass
    values to additional functions/libraries that aren't assumed to 
    run within the defined program. """

parser = argparse.ArgumentParser()
parser.add_argument(
    '--number_nodes',
    type=int,
    default=1,
    help="number of nodes in hidden layer"
)
parser.add_argument(
    '--activation_function',
    type=str,
    default='tanh',
    help="short hand for network activation function to use"
)
args = parser.parse_args()
args = vars(args)
HyperParameters = {}
HyperParameters['number_nodes'] = int(args['number_nodes'])
HyperParameters['activation_function'] = str(args['activation_function'])

""" Functions that define the NN architecture
    ----------------------------------------- """

def posterior_mean_field(kernel_size, bias_size=0, dtype=None):
    '''Specify the surrogate posterior over `kernel` and `bias`.'''
    n = kernel_size + bias_size
    c = np.log(np.expm1(1.))
    return tf.keras.Sequential([
        tfp.layers.VariableLayer(2 * n, dtype=dtype),
        tfp.layers.DistributionLambda(lambda t: tfd.Independent(
            tfd.Normal(
                loc=t[..., :n],
                scale=1e-5 + tf.nn.softplus(c + t[..., n:])
            ),
            reinterpreted_batch_ndims=1)),
    ])

def prior_trainable(kernel_size, bias_size=0, dtype=None):
    '''Specify the prior over `kernel` and `bias`.'''
    n = kernel_size + bias_size
    return tf.keras.Sequential([
        tfp.layers.VariableLayer(n, dtype=dtype),
        tfp.layers.DistributionLambda(
            lambda t: tfd.Independent(
                tfd.Normal(loc=t, scale=1),
                reinterpreted_batch_ndims=1)),
    ])

def define_model(HyperParameters) : 
    '''Define a single layer (plus formating layers) BANN,
    with fixed varience Gaussian distribution assumed for targets.
    
    HyperParameters : a dictionary giving the number of nodes 
                      and activation function to use in the hidden layer.
    '''
    model = tf.keras.Sequential([
        ## Initilization layer
        tf.keras.layers.Input(
            shape=(1,)
        ),
        ## Interpretation layer
        tfp.layers.DenseVariational(
            units=HyperParameters['number_nodes'],
            make_posterior_fn=posterior_mean_field,
            make_prior_fn=prior_trainable,
            activation=HyperParameters['activation_function'],
            kl_weight=1
        ),
        ## Funnel layer
        tfp.layers.DenseVariational(
            units=1,
            make_posterior_fn=posterior_mean_field,
            make_prior_fn=prior_trainable,
            kl_weight=1
        ),
        ## Distribution layer
        tfp.layers.DistributionLambda(
            lambda t: tfd.Normal(loc=t,scale=0.5)
        )
    ])
    ## Specify the training loss function
    negloglik = lambda y, p_y: -p_y.log_prob(y)
    ## Set training parameters
    _ = model.compile(
        loss=negloglik,
        optimizer=tf.keras.optimizers.Adam(lr=0.03)
    )
    return(model)

def draw_fit() :
    '''Quick sketch of curve fit and training data'''
    _ = plt.figure(facecolor='lavender')
    _ = plt.plot(x,y,'o')
    _ = plt.fill_between(
        x=x_tst,
        y1=yhat_mu+yhat_sigma,
        y2=yhat_mu-yhat_sigma,
        color=cpal[2],alpha=0.5
    )
    _ = plt.fill_between(
        x=x_tst,
        y1=yhat_mu+2*yhat_sigma,
        y2=yhat_mu-2*yhat_sigma,
        color=cpal[2],alpha=0.5
    )
    _ = plt.plot(
        x_tst,yhat_mu,
        color=cpal[2]
    )
    _ = plt.xlabel('Time')

    figlab = (
        'Model_Fit_NN'
        + str(HyperParameters['number_nodes'])
        + '_AF'+HyperParameters['activation_function']
        + '.png'
    )
    _ = plt.savefig(OUTDIR+'/'+figlab)

# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>:: #

""" Fit and evaluate model
    ----------------------
    When this program is run through Sagemaker environment variables are set
    stating an S3 location for training and testing data: 
    `SM_CHANNEL_TRAIN` and `SM_CHANNEL_TEST`.
    These directories can contain multiple files and arranged according to
    the desired training strategy, although in this example use the simplest
    possible arrangement for clarity. 
    
    Evaluation metrics to be used for hyperparameter tuning need to be
    printed out so that they can be found and used by the Sagemaker program.
    """ 
training_data_files = os.listdir(os.environ['SM_CHANNEL_TRAIN'])
D_train = pd.read_csv(
    os.environ['SM_CHANNEL_TRAIN']
    +'/'
    +training_data_files[0]
)
#D_train = pd.read_csv('training_data.csv')
x = D_train['Input'].values
y = D_train['Target'].values

## Train model
model = define_model(HyperParameters) 
_ = model.fit(
    x,y,epochs=1500,verbose=0
)

## Check model fit
test_data_files = os.listdir(os.environ['SM_CHANNEL_TEST'])
D_test = pd.read_csv(
    os.environ['SM_CHANNEL_TEST']
    +'/'
    +test_data_files[0]
)
#D_test = pd.read_csv('test_data.csv')
x_tst = D_test['Input'].values
nsamples = 100 
P = pd.DataFrame(
    index=x_tst,
    columns=range(nsamples)
)
for s in range(nsamples) :
    P.loc[:,s] = model(x_tst.reshape(-1,1)).mean().numpy().flatten()
    
yhat_mu = P.mean(1)
yhat_sigma = P.std(1)

## Show model fit
draw_fit()

## Report Mean Absolute Error
test_errors = yhat_mu - D_test['Target'].values 
MAE = np.mean(np.abs(test_errors))
STD = np.std(test_errors)
CONF = np.mean(yhat_sigma)
te_std_corr = np.corrcoef(yhat_sigma, np.abs(test_errors))[0,1]
print("RESULT MAE: {0:0.8f}".format(MAE))
print("RESULT STD: {0:0.8f}".format(STD))
print("RESULT CONF: {0:0.8f}".format(CONF))
print("RESULT COR: {0:0.8f}".format(te_std_corr)) 

# ::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>::::<>:: #
